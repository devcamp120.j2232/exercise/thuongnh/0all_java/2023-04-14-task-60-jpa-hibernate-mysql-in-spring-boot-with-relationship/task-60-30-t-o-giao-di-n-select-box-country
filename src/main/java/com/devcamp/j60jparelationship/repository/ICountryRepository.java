package com.devcamp.j60jparelationship.repository;

import com.devcamp.j60jparelationship.model.CCountry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);  // muốn custom hóa phương thức này để truy vấn dữ liệu theo cách riêng của mình,
}
